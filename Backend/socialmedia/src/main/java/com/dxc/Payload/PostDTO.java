package com.dxc.Payload;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostDTO {
	private long id;

	private String username;

	private String medialink;

	private String mediatype;

	private Date createDate;

	private Date updateDate;

	private int clicks;

}
