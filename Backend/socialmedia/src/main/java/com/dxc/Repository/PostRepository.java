package com.dxc.Repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.dxc.entity.Post;
import com.dxc.entity.User;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
//	@Query("SELECT u FROM User u WHERE u.status = 1")
//	Collection<User> findAllActiveUsers();
}
