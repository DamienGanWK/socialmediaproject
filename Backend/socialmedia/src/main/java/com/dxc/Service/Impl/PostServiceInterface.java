package com.dxc.Service.Impl;

import com.dxc.Payload.PostDTO;
import com.dxc.Payload.PostResponse;

public interface PostServiceInterface {
	PostDTO createPost(PostDTO postDto);
	
	PostResponse getAppPosts(int pageNo, int pageSize,String sortBy,String sortDir);
	
	PostDTO getPostById(Long id);
	
	PostDTO updatePost(PostDTO postDto, Long id);
	
	void deletePostById(Long id);

}
