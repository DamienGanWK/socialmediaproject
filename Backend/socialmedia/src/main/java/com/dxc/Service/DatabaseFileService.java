package com.dxc.Service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.dxc.Repository.DatabaseFileRepository;
import com.dxc.entity.DbFile;
import com.dxc.exception.FileNotFoundException;
import com.dxc.exception.FileStorageException;


@Service
public class DatabaseFileService {
	@Autowired
	private DatabaseFileRepository dbFileRespository;
	
	public DbFile storeFile(MultipartFile file) {
		String fileName=StringUtils.cleanPath(file.getOriginalFilename());
		try {
			if(fileName.contains("..")) {
				throw new FileStorageException(
						"Sorry! FileName contains invalid path Sequence"+fileName
						);
			}
			DbFile dbFile=new DbFile(fileName,file.getContentType(),file.getBytes());
		    return dbFileRespository.save(dbFile);
		}catch(IOException e) {
			throw new FileStorageException("Could not Store File"+fileName+"please try again",e);
		}
		
	}
	
	public DbFile getFile(String fileId) {
		return dbFileRespository.findById(fileId)
				.orElseThrow(
						()->new FileNotFoundException(
								"File Not found with ID"+fileId
								)
						);
	}
	
}
