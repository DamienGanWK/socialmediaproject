package com.dxc.Service.Impl;


import com.dxc.Payload.UserDTO;
import com.dxc.Payload.UserResponse;

public interface UserServiceInterface{
	//List<User> getAllUsers();
	
	UserDTO createUser(UserDTO userDto);
	
	UserResponse getAppUsers(int pageNo, int pageSize,String sortBy,String sortDir);
	
	UserDTO getUserById(Long id);
	
	UserDTO updateUser(UserDTO userDto, Long id);
	
	void deleteUserById(Long id);
	
	//User save(UserRegistrationDto registrationDto);
}
