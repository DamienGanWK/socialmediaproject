package com.dxc.Service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.dxc.Payload.UserDTO;
import com.dxc.Payload.UserResponse;
import com.dxc.Repository.UserRepository;
import com.dxc.Service.Impl.UserServiceInterface;
import com.dxc.entity.User;
import com.dxc.exception.ResourceNotFoundException;


@Service
public class UserServiceImpl implements UserServiceInterface{


	@Autowired
	private UserRepository userRespository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	private UserDTO mapToDto(User user) {
		UserDTO userDto = new UserDTO();
		userDto.setId(user.getId());
		userDto.setUsername(user.getUsername());
		userDto.setFirstname(user.getFirstname());
		userDto.setLastname(user.getLastname());
		userDto.setEmail(user.getEmail());
		userDto.setPassword(passwordEncoder.encode(user.getPassword()));
		//userDto.setPassword(user.getPassword());
		userDto.setRole(user.getRole());

		return userDto;
	}

	public User mapToEntity(UserDTO userDto) {
		User user = new User();
		user.setId(null);
		user.setUsername(userDto.getUsername());
		user.setFirstname(userDto.getFirstname());
		user.setLastname(userDto.getLastname());
		user.setEmail(userDto.getEmail());
		user.setPassword(passwordEncoder.encode(userDto.getPassword()));
		user.setRole(userDto.getRole());

		return user;

	}

	// implementing Create Userblog
	public UserDTO createUser(UserDTO userDto) {
		User user = mapToEntity(userDto);
		User newUser = userRespository.save(user);

		// convert entity to DTO
		UserDTO userResponse = mapToDto(newUser);
		return userResponse;
	}

	// implementing the Get UserBlog
	@Override
	public UserResponse getAppUsers(int pageNo, int pageSize, String sortBy, String sortDir) {
		Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
				: Sort.by(sortBy).descending();

		Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
		Page<User> users = userRespository.findAll(pageable);

		// get content from page object
		List<User> listOfUsers = users.getContent();

		List<UserDTO> content=listOfUsers.stream().map(
				user -> mapToDto(user)).collect(Collectors.toList()
				);
		
		UserResponse userResponse = new UserResponse();
		userResponse.setContent(content);
		userResponse.setPageNo(users.getNumber());
		userResponse.setPageSize(users.getSize());
		userResponse.setTotalElement(users.getTotalElements());
		userResponse.setTotalPages(users.getTotalPages());
		userResponse.setLast(users.isLast());
		return userResponse;
	}

	@Override
	public UserDTO getUserById(Long id) {
		User user = userRespository.findById(id).orElseThrow(
				()->new ResourceNotFoundException("POST", "id", id));
		return mapToDto(user);
	}

	@Override
	public UserDTO updateUser(UserDTO userDto, Long id) {
		User user = userRespository.findById(id).orElseThrow(
				()->new ResourceNotFoundException("POST", "id", id));
		user.setUsername(userDto.getUsername());
		user.setFirstname(userDto.getFirstname());
		user.setLastname(userDto.getLastname());
		user.setEmail(userDto.getEmail());
		user.setPassword(userDto.getPassword());

		
		User updateUser = userRespository.save(user);
		return mapToDto(updateUser);	
	}

	@Override
	public void deleteUserById(Long id) {
		User user = userRespository.findById(id).orElseThrow(
				()->new ResourceNotFoundException("POST", "id", id));
		userRespository.delete(user);
	}


}