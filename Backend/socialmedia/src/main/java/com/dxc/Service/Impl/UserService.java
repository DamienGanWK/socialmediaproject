package com.dxc.Service.Impl;


import org.springframework.security.core.userdetails.UserDetailsService;

import com.dxc.Payload.UserRegistrationDto;
import com.dxc.entity.User;



public interface UserService extends UserDetailsService{
	User save(UserRegistrationDto registrationDto);
}
