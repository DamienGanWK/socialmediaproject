package com.dxc.Respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dxc.entity.DbFile;

@Repository
public interface DatabaseFileRepository extends JpaRepository<DbFile,String>{

}
