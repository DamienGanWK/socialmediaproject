package com.dxc.Respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dxc.entity.User;

@Repository
public interface UserRespository extends JpaRepository<User, Long>{
}