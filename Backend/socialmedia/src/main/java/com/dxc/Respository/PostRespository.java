package com.dxc.Respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dxc.entity.Post;

@Repository
public interface PostRespository extends JpaRepository<Post, Long> {

}
