package com.dxc.entity;

import java.util.Date;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "image_model")
@AllArgsConstructor
@Data
@NoArgsConstructor
@ToString
public class ImageModel {
	@Id
	@Column(name = "id")
	@GeneratedValue
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "type")
	private String type;
	
	@Column(name = "caption")
	private String caption;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "click")
	private Long click;
	
	@Column(name = "postDate")
	private Date currentdate;

	@Lob
	@Column(name = "pic")
	private byte[] pic;

//Custom Construtor
	public ImageModel(String name, String type, String caption, String username, Long click, Date currentdate, byte[] pic) {
		this.name = name;
		this.type = type;
		this.caption = caption;
		this.username = username;
		this.click = click;
		this.currentdate = currentdate;
		this.pic = pic;
	}
}
