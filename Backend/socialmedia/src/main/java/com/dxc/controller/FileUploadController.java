package com.dxc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.dxc.Payload.Response;
import com.dxc.Service.DatabaseFileService;
import com.dxc.entity.DbFile;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class FileUploadController {
	
	@Autowired
	private DatabaseFileService fileStorageService;
	
	@PostMapping("/uploadFile")
	public Response uploadFile(@RequestParam("file")MultipartFile file) {
		DbFile fileName=fileStorageService.storeFile(file);
		
		String fileDownloadUri=ServletUriComponentsBuilder.fromCurrentContextPath()
				               .path("/downloadFile/")
				               .path(fileName.getFileName())
				               .toUriString();
		return new Response(fileName.getFileName(), fileDownloadUri,file.getContentType(),file.getSize());
	}
}
