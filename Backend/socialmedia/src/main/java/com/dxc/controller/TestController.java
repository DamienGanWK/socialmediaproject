package com.dxc.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.dxc.Repository.ImageRepository;
import com.dxc.entity.ImageModel;
import com.dxc.utils.AppConst;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "check")

public class TestController {

    @Autowired
    ImageRepository imageRepository;

    @PostMapping("/upload")
    public ImageModel uploadImage(@RequestParam("myFile") MultipartFile file) throws IOException {

        ImageModel img = new ImageModel( 
        		file.getOriginalFilename(),
        		file.getContentType(),
        		null,
        		null,
        		(long) 0,
        		new Date(),
        		file.getBytes() 
        		);

        final ImageModel savedImage = imageRepository.save(img);

        System.out.println("Image saved");

        return savedImage;
    }
    
    //get all images
	@GetMapping("/getimages")
	public List<ImageModel> findAllImg(){
		return imageRepository.findAll();
	}
	
	//get a image by name
	@GetMapping(path = { "/get/{imageName}" })
	public ImageModel getImage(@PathVariable("imageName") String imageName) throws IOException {
		final Optional<ImageModel> retrievedImage = imageRepository.findByName(imageName);
		ImageModel img = new ImageModel(retrievedImage.get().getId(),
				retrievedImage.get().getName(), 
				retrievedImage.get().getType(),
				retrievedImage.get().getCaption(), 
				retrievedImage.get().getUsername(),
				retrievedImage.get().getClick(), 
				retrievedImage.get().getCurrentdate(),
				retrievedImage.get().getPic());
		return img;
	}
	
	@DeleteMapping(path = { "/getimages/{id}" })
	void deleteImage(@PathVariable("id") Long id) throws IOException{
		imageRepository.deleteById(id);
	}
	
	@PutMapping(path = { "/getimages/{id}" })
	void updateCaption(@RequestBody ImageModel imagemodel,@PathVariable("id") Long id) {
		imageRepository.save(imagemodel);
	}
	
	@PutMapping(path = { "/getimages/{id}/{caption}" })
	void updateUserCaption(@PathVariable("id") Long id,@PathVariable("caption") String caption) {
		Optional<ImageModel> img = imageRepository.findById(id);
		ImageModel imagemodel = new ImageModel(img.get().getId(),
				img.get().getName(), 
				img.get().getType(),
				img.get().getCaption(), 
				img.get().getUsername(),
				img.get().getClick(), 
				img.get().getCurrentdate(),
				img.get().getPic());
		
		imagemodel.setCaption(caption);

		imageRepository.save(imagemodel);
	}
	
	@PutMapping(path = { "/getimages/{id}/click" })
	void updateClick(@PathVariable("id") Long id) {
		Optional<ImageModel> img = imageRepository.findById(id);
		ImageModel imagemodel = new ImageModel(img.get().getId(),
				img.get().getName(), 
				img.get().getType(),
				img.get().getCaption(), 
				img.get().getUsername(),
				img.get().getClick(), 
				img.get().getCurrentdate(),
				img.get().getPic());
		
		imagemodel.setClick(imagemodel.getClick()+1);
		
		imageRepository.save(imagemodel);
	}
	
	
//	// compress the image bytes before storing it in the database
//	public static byte[] compressBytes(byte[] data) {
//		Deflater deflater = new Deflater();
//		deflater.setInput(data);
//		deflater.finish();
//		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
//		byte[] buffer = new byte[1024];
//		while (!deflater.finished()) {
//			int count = deflater.deflate(buffer);
//			outputStream.write(buffer, 0, count);
//		}
//		try {
//			outputStream.close();
//		} catch (IOException e) {
//		}
//		System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);
//		return outputStream.toByteArray();
//	}
//	// uncompress the image bytes before returning it to the angular application
//	public static byte[] decompressBytes(byte[] data) {
//		Inflater inflater = new Inflater();
//		inflater.setInput(data);
//		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
//		byte[] buffer = new byte[1024];
//		try {
//			while (!inflater.finished()) {
//				int count = inflater.inflate(buffer);
//				outputStream.write(buffer, 0, count);
//			}
//			outputStream.close();
//		} catch (IOException ioe) {
//		} catch (DataFormatException e) {
//		}
//		return outputStream.toByteArray();
//	}
	
}
