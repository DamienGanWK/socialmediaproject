package com.dxc.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dxc.Payload.UserDTO;
import com.dxc.Payload.UserResponse;
import com.dxc.Repository.UserRepository;
import com.dxc.Service.Impl.UserServiceInterface;
import com.dxc.entity.User;
import com.dxc.utils.AppConst;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("api/users")
public class UserController {

	@Autowired
	private UserServiceInterface userService;
	
	@Autowired
	private UserRepository userRepository;

	// create blog user
	@PostMapping
	public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDto){
		return new ResponseEntity<UserDTO>(userService.createUser(userDto),HttpStatus.CREATED);
	}

	// get all blog users REST API
	@GetMapping
	@PreAuthorize("hasRole('ADMIN')")
	public UserResponse getAllBlogs(@RequestParam(value="pageNo",defaultValue = AppConst.DEFAULT_PAGE_NUMBER, required = false) int pageNo,
			@RequestParam(value="pageSize",defaultValue = AppConst.DEFAULT_PAGE_SIZE, required = false) int pageSize,
			@RequestParam(value="sortBy",defaultValue = AppConst.DEFAULT_SORT_BY, required =   false) String sortBy,
			@RequestParam(value="sortDir",defaultValue= AppConst.DEFAULT_SORT_DIRECTION, required = false) String sortDir) {
		
		return userService.getAppUsers(pageNo, pageSize, sortBy, sortBy);
	}
	
	//Get all User by Id
	@GetMapping("/{id}")
	public ResponseEntity<UserDTO> getUserById(@PathVariable(name="id")long id){
		return ResponseEntity.ok(userService.getUserById(id));
	}
	
	//Update/PUT user by REST
	@PutMapping("/{id}")
	public ResponseEntity<UserDTO> updateUser(@RequestBody UserDTO userDto, @PathVariable(name="id")Long id){
		UserDTO userResponse = userService.updateUser(userDto, id);
		return new ResponseEntity<UserDTO>(userResponse,HttpStatus.OK);
	}
	
	//Delete user by REST
	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<String> deleteUserById(@PathVariable(name="id") Long id){
		userService.deleteUserById(id);
		return new ResponseEntity<String>("User entity deleted successfully",HttpStatus.OK);
	}
	
    @GetMapping("/users")
    public List<User> getUsers() {
        return userRepository.findAll();
    }
    
    @PostMapping("/users")
    void addUser(@RequestBody User user) {
        userRepository.save(user);
    }
    
    @DeleteMapping("/users/{id}")
    void deleteUser(@PathVariable(value="id") long id) {
        userRepository.deleteById(id);
    }
    
    @PutMapping("/users")
    void updateUser(@RequestBody User user) {
    	userRepository.save(user);
    }
}
