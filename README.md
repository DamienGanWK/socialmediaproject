## App features:
- A standard list of posts will be displayed for all users.
- Page size will be 10 postss per page.
- Clicking a video/hyperlink will open a new tab (_blank)
- All posts will track number of clicks.

- Users can sign up for a new account.
- Users can login with existing account.
- Users can create a post and it will be visible as the latest post for all users.
- Users can update/delete their own posts.
- Posts can include a link or file to be uploaded.

- Admins can update/delete all posts.
- Admins can update/delete users.

## Implementation:
1.	Create login and registration functions
>     a.	Create REST API for creating new users and getting user info for login verification
>     b.	Create a login and registration page
>     c.	Integrate front and backend

2.	Create user page
>     a.	Create REST API to get all posts from database, with 10 dummy posts in database
>     b.	Create user page
>     c.	Implement create, update and delete of user posts

3.	Create Admin functions
>     a.	Create REST API to update and delete users/posts
>     b.	Create 2 pages, one to manage users, another to manage posts


## Frontend technologies used:
- Angular CLI: 14.2.1
- Node: 16.17.0
- Package Manager: npm 8.15.0
- Angular: 14.2.0
- Bootstrap 4
- VSCode IDE


## Backend technologies used:
- JDK 1.8
- Maven
- Springboot
- SpringData JPA 
- Spring REST
- STS4 IDE
- MySQL Database
- Embedded Tomcat
- Postman for REST functions


