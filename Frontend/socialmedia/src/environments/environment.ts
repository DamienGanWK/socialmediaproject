// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  firebaseConfig : {
    apiKey: "AIzaSyAJ09UcX-mNotQns4Wz_ZHXqFgxvXbg_PU",
    authDomain: "social-media-proj.firebaseapp.com",
    projectId: "social-media-proj",
    storageBucket: "social-media-proj.appspot.com",
    messagingSenderId: "373726767047",
    appId: "1:373726767047:web:fa55ac68a8f00040d59be2",
    measurementId: "G-EMP6D1CHGH"
}

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
