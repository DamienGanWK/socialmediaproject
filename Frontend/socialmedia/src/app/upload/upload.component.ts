import { Component, OnInit } from '@angular/core';
import { HttpBackend, HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../login/auth.service';
import { ImageService } from '../service/image.service';
import { Image } from '../model/image';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  private httpClient: HttpClient;

  constructor(
    private authenticationService: AuthenticationService,
    private imageService: ImageService,
    handler: HttpBackend) 
    { 
    this.httpClient = new HttpClient(handler);
    this.image = new Image();

  }

  title = 'ImageUploaderFrontEnd';

  public selectedFile: any;
  public event1: any;
  imgURL: any;
  receivedImageData: any;
  base64Data: any;
  convertedImage: any;
  caption: any;
  username!: string;
  image: Image;


  public onFileChanged(event) {
    console.log(event);
    this.selectedFile = event.target.files[0];

    // Below part is used to display the selected image
    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (event2) => {
      this.imgURL = reader.result;
    };
  }

  // This part is for uploading
  onUpload(
    commentInput:HTMLTextAreaElement
  ) {
    this.caption = commentInput.value;
    console.log(this.caption);
    console.log(this.username);

    const uploadData = new FormData();
    if(this.selectedFile != null){
      uploadData.append('myFile', this.selectedFile, this.selectedFile.name.slice(0,-4));
    }else{
      alert("Please upload a media file")
    }
    console.log(uploadData);

    const headers = new HttpHeaders({
      
    })
    

    this.httpClient.post('http://localhost:8080/check/upload', uploadData,{headers:headers})
      .subscribe(
        res => {
          console.log(res);
          this.receivedImageData = res;
          console.log(this.receivedImageData.id)
          this.base64Data = this.receivedImageData.pic;
          this.receivedImageData.username = this.username;
          this.receivedImageData.caption = this.caption;
          this.convertedImage = 'data:image/jpeg;base64,' + this.base64Data;

          this.imageService.updateCaption(this.receivedImageData.id,this.receivedImageData).subscribe(data=>{
            alert("Image posted");
            location.reload();
          })

        },
        err => console.log('Error Occured during saving: ' + err)
      );




  }



  ngOnInit(): void {
    this.username = this.authenticationService.getLoggedInUserName();
  }

}
