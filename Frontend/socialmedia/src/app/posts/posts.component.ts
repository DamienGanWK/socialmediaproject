import { Component, OnInit } from '@angular/core';
import { FirebaseTSAuth } from 'firebasets/firebasetsAuth/firebaseTSAuth';
import { FirebaseTSFirestore } from 'firebasets/firebasetsFirestore/firebaseTSFirestore';
import { FirebaseTSStorage } from 'firebasets/firebasetsStorage/firebaseTSStorage'
import { FirebaseTSApp } from 'firebasets/firebasetsApp/firebaseTSApp';
import { MatDialogRef } from '@angular/material/dialog';

import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpBackend, HttpClient } from '@angular/common/http';
import { UploadService } from '../upload.service';
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  file: File = null; 
  selectImageFile!: File;
  auth = new FirebaseTSAuth();
  firestore = new FirebaseTSFirestore();
  storage = new FirebaseTSStorage();

  private http: HttpClient;


  constructor(
    private dialog: MatDialogRef<PostsComponent>,
    public fb: FormBuilder, 
    private uploadService: UploadService,
    handler: HttpBackend
    ) {
      this.http = new HttpClient(handler);
    }

    

  ngOnInit(): void {
  }

  onFilechange(event: any) {
    console.log(event.target.files[0])
    this.file = event.target.files[0]
  }
  
  upload() {
    if (this.file) {
      this.uploadService.uploadfile(this.file).subscribe(resp => {
        alert("Uploaded")
      })
    } else {
      alert("Please select a file first")
    }
  }
 /*-------------------------------------------------------*/

  onPhotoSelected(
    photoSelector: HTMLInputElement
  ) {
    this.selectImageFile = photoSelector.files[0]
    let fileReader = new FileReader();
    fileReader.readAsDataURL(this.selectImageFile);
    fileReader.addEventListener(
      "loadend",
      ev => {
        let reableString = fileReader.result!.toString();
        let postPreviewImage = <HTMLImageElement>document.getElementById("post-preview-image");
        postPreviewImage.src = reableString;
      }
    )
  }

  onPostClick(
    commentInput: HTMLTextAreaElement
  ) {
    let comment = commentInput.value;
    if(comment.length<=0) return;
    if(this.selectImageFile){
      this.uploadImagePost(comment)
    }else{
      this.uploadPost(comment)
    }
  }
  /*-------------------------------------------------------*/

  uploadImagePost(
    comment: string
  ) {
    let postId = this.firestore.genDocId();
    this.storage.upload(
      {
        uploadName: "Upload image post",
        path: ["post", postId, "image"],
        data: {
          data: this.selectImageFile
        },
        onComplete: (downloadUrl) => {
          //alert(downloadUrl)
          this.firestore.create({
            path: ["post", postId],
            data: {
              comment: comment,
              createId: this.auth.getAuth().currentUser!.uid,
              imageUrl: downloadUrl,
              timestamp: FirebaseTSApp.getFirestoreTimestamp()
            },
            onComplete: (docId) => {
              this.dialog.close();
            }
          })
        }
      }
    )
  }
  /*-------------------------------------------------------*/

  uploadPost(
    comment: string
  ) {
    this.firestore.create({
      path: ["post"],
      data: {
        comment: comment,
        createId: this.auth.getAuth().currentUser!.uid,
        timestamp: FirebaseTSApp.getFirestoreTimestamp()
      },
      onComplete: (docId) => {
        this.dialog.close();
      }
    })
  }

}
