import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { FirebaseTSFirestore, Limit, OrderBy } from 'firebasets/firebasetsFirestore/firebaseTSFirestore';
import { PostsComponent } from '../posts/posts.component';
import { UploadComponent } from '../upload/upload.component';

@Component({
  selector: 'app-postfeed',
  templateUrl: './postfeed.component.html',
  styleUrls: ['./postfeed.component.css']
})
export class PostfeedComponent implements OnInit {

  firestore = new FirebaseTSFirestore();
  post:PostData[]=[]

  constructor(private dialog:MatDialog) { }

  ngOnInit(): void {
    this.getPosts();
  }

  onCreatePostClick(){
    this.dialog.open(PostsComponent)

  }

  getPosts(){
    this.firestore.getCollection({
      path:["post"],
      where:[
        new OrderBy("timestamp","desc"),
        new Limit(10)
      ],
      onComplete:(result)=>{
        result.docs.forEach(
          doc=>{
            let post = <PostData>doc.data();
            this.post.push(post);
          }
        )
      },
      onFail:err=>{

      }
    })
  }

}

export interface PostData{
  comment:string;
  createId: string;
  imageUrl?:string;
  timestamp:string;
}

