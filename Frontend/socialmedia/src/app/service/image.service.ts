import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Image } from '../model/image';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private usersUrl: string;

  constructor(private http: HttpClient) {
    this.usersUrl = 'http://localhost:8080/check/getimages';
  }

  public findAll(): Observable<Image[]> {
    return this.http.get<Image[]>(this.usersUrl);
  }

  public delete(i: any) {
    console.log(this.usersUrl + '/' + i);
    return this.http.delete(this.usersUrl + '/' + i);
  }

  public updateCaption(i: any, image: any) {
    console.log(this.usersUrl + '/' + i);
    return this.http.put(this.usersUrl + '/' + i, image);
  }

  public updateUserCaption(i: any, caption: string, image: any) {
    console.log(this.usersUrl + '/' + i + '/' + caption);
    return this.http.put(this.usersUrl + '/' + i + '/' + caption, image);
  }

  public updatePostName(i: any, name: string, image: any) {
    console.log(this.usersUrl + '/' + i + '/updatename/' + name);
    return this.http.put(this.usersUrl + '/' + i + '/updatename/' + name, image);
  }

  public updateClick(i: any, image: any) {
    console.log(this.usersUrl + '/' + i);
    return this.http.put(this.usersUrl + '/' + i + '/click', null);
  }
}
