import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { UserService } from '../service/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users!: User[];
  user: User;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private userService: UserService
    ) {
    this.user = new User();
  }

  ngOnInit() {
    this.userService.findAll().subscribe(data => {
      this.users = data;
    });
  }

  onDeleteClick(id:any){
    this.userService.delete(id).subscribe(data => {
      alert("User deleted");
      location.reload();
    });
  }

  onUpdateClick(user: any) {
    this.userService.update(user).subscribe(result => console.log(result));
    //location.reload();
  }

  onSubmit() {
    this.userService.save(this.user).subscribe(result => this.gotoUserList());
  }

  gotoUserList() {
    this.router.navigate(['/users']);
  }


}
