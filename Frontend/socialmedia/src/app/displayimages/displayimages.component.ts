import { Component, Input, OnInit } from '@angular/core';
import { ImageData } from '../hello-world/hello-world.component';
import { AuthenticationService } from '../login/auth.service';
import { ImageService } from '../service/image.service';
import { Image } from '../model/image';
import { UploadComponent } from '../upload/upload.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-displayimages',
  templateUrl: './displayimages.component.html',
  styleUrls: ['./displayimages.component.css']
})
export class DisplayimagesComponent implements OnInit {

  username!: string;
  image: Image;
  caption: string = '';
  name: string = '';

  @Input() imageData!: ImageData;
  constructor(
    private authenticationService: AuthenticationService,
    private dialog: MatDialog,
    private imageService: ImageService
  ) {
    this.image = new Image();
  }

  ngOnInit(): void {
    this.username = this.authenticationService.getLoggedInUserName();
  }

  onDeleteClick(id: string) {
    this.imageService.delete(id).subscribe(data => {
      alert("Post deleted");
      location.reload();
    })
  }

  onLikeClick(id: string, image: any) {
    this.imageService.updateClick(id, null).subscribe(data => {
      alert("Liked");
      location.reload();

    })
  }

  onCreatePostClick(id: string, image: any) {
    console.log(id,this.caption,this.name);
    this.imageService.updateUserCaption(id,this.caption,null).subscribe(data=>{
      alert("Caption updated");
    })
    this.imageService.updatePostName(id,this.name,null).subscribe(data=>{
      alert("Post name updated");
    })
    location.reload();
  }


  validURL(caption:string) {
    let pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
      //console.log(!!pattern.test(caption));
    return !!pattern.test(caption);
  }




}
