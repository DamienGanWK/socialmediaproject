import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayimagesComponent } from './displayimages.component';

describe('DisplayimagesComponent', () => {
  let component: DisplayimagesComponent;
  let fixture: ComponentFixture<DisplayimagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayimagesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DisplayimagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
