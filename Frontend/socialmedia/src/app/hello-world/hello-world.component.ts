import { HttpBackend, HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HelloWordService } from '../hello-word.service';
import { AuthenticationService } from '../login/auth.service';
import { Message } from '../message';
import { PostsComponent } from '../posts/posts.component';
import { UploadComponent } from '../upload/upload.component';
import { PageEvent } from '@angular/material/paginator';


@Component({
  selector: 'app-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.css']
})
export class HelloWorldComponent implements OnInit {

  message!: string;

  selectedFile!: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  imageName: any;
  images: ImageData[] = [];
  images2: ImageData[] = [];


  username!: string;


  private httpClient: HttpClient

  constructor(
    private helloWorldService: HelloWordService,
    private dialog: MatDialog,
    private authenticationService: AuthenticationService,
    handler: HttpBackend
  ) {
    this.httpClient = new HttpClient(handler);
  }

  ngOnInit() {
    this.username = this.authenticationService.getLoggedInUserName();
    this.getImage();
    this.images2 = this.images;
    console.log("HelloWorldComponent");
    this.helloWorldService.helloWorldService().subscribe((result) => {
      this.message = result.content;
    });
  }


onPageLoad() {
  this.images2 =  this.images.slice(0,10);
}

  onPageChange($event) {
    console.log($event)
    //this.images2 =  this.images.slice(0,10);
    this.images2 =  this.images.slice($event.pageIndex*$event.pageSize, $event.pageIndex*$event.pageSize + $event.pageSize);
    window.scroll(0,0);
    console.log($event.pageIndex*$event.pageSize, $event.pageIndex*$event.pageSize + $event.pageSize)
  }



  //Gets called when the user clicks on retieve image button to get the image from back end
  getImage() {

    //Make a call to Sprinf Boot to get the Image Bytes.
    // this.httpClient.get('http://localhost:8080/check/get/' + this.imageName)
    this.httpClient.get('http://localhost:8080/check/getimages')
      .subscribe(
        res => {
          console.log(Object.values(res))

          Object.values(res).forEach(i => {
            // this.imageName = i.name;
            // this.retrieveResonse = i;
            // this.base64Data = this.retrieveResonse.pic;
            // this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
            // //console.log(this.retrievedImage)
            this.images.push(i);
            //this.base64Data = this.images[1].pic;
            //this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
          })
          this.images.reverse();
        }
      );
  }

  onCreatePostClick() {
    //this.dialog.open(PostsComponent)
    this.dialog.open(UploadComponent)
  }

}

export interface ImageData {
  imageUrl?: string;
  pic: string;
  name: string;
  id: string;
  type: string;
  username: string;
  caption: string;
  click: string;
  currentdate: string;
}
