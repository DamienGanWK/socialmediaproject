import { Component, Input, OnInit } from '@angular/core';
import { ImageData } from '../hello-world/hello-world.component';
import { Image } from '../model/image';
import { ImageService } from '../service/image.service';


@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  images!: Image[];
  image: Image;

  constructor(
    private imageService: ImageService
  ) { 
    this.image = new Image();
  }

  ngOnInit(): void {
    this.imageService.findAll().subscribe(data => {
      this.images = data;
    });
  }

  onDeleteClick(id:string){
    this.imageService.delete(id).subscribe(data=>{
      alert("Post deleted");
      location.reload();
    })
  }

}
