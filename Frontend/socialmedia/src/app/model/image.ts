export class Image {
    id!: string;
    name!: string;
    pic!: string;
    type! : string;
    username! : string;
    caption!: string;
    click! : string;
    currentdate! : string
}