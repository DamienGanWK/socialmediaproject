export class User {
    id!: string;
    username!: string;
    firstname!: string;
    lastname! : string;
    password!: string;
    email!: string;
    role! :string;
}
