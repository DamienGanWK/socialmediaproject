import { Component, OnInit } from '@angular/core';
import { FirebaseTSAuth } from 'firebasets/firebasetsAuth/firebaseTSAuth';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../login/auth.service';
import { FormBuilder, FormControl, FormGroup, NgForm } from '@angular/forms';
import { NewserviceService } from '../newservice/newservice.service';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-authenticator',
  templateUrl: './authenticator.component.html',
  styleUrls: ['./authenticator.component.css']
})
export class AuthenticatorComponent implements OnInit {
  firstname: string = "";
  lastname: string = "";
  email: string = "";
  err:any;
  status: string ="";

  username!: string;
  password! : string;
  errorMessage = 'Invalid Credentials';
  successMessage!: string;
  invalidLogin = false;
  loginSuccess = false;
  
  forgotFlag = false;
  createFlag = false;

  state = AuthenticatorCompState.LOGIN;
  firebasetsAuth: FirebaseTSAuth

  constructor(
    private bottomSheetRef: MatBottomSheetRef,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userservice:NewserviceService
    ) {   
    this.firebasetsAuth = new FirebaseTSAuth
  }

  ngOnInit(): void {
    this.userservice.updateUser();
  }

  forgotPw() {
    this.forgotFlag = true;
    this.createFlag = false;
    this.state = AuthenticatorCompState.FORGOT_PASSWORD;
  }

  login() {
    this.forgotFlag = false;
    this.createFlag = false;
    this.state = AuthenticatorCompState.LOGIN;
  }

  createUser() {
    this.createFlag = true;
    this.forgotFlag = false;
    this.state = AuthenticatorCompState.REGISTER;
  }

  isLoginState() {
    return this.state == AuthenticatorCompState.LOGIN
  }

  getStateText() {
    switch (this.state) {
      case AuthenticatorCompState.LOGIN: return "Login"
      case AuthenticatorCompState.FORGOT_PASSWORD: return "Forgot Password"
      case AuthenticatorCompState.REGISTER: return "Register"
    }
  }

  isNotEmpty(text: String) {
    return text != null && text.length > 0
  }

  isMatch(text: String, compareWith: String) {
    return text == compareWith
  }

  onRegisterClick(
    registerEmail: HTMLInputElement,
    registerPassword: HTMLInputElement,
    registerConfirmPassword: HTMLInputElement
  ) {
    let email = registerEmail.value;
    let password = registerPassword.value;
    let confirmpassword = registerConfirmPassword.value;

    if (this.isNotEmpty(email) &&
      this.isNotEmpty(password) &&
      this.isMatch(password, confirmpassword)
    ) {
      this.firebasetsAuth.createAccountWith({
        email: email,
        password: password,
        onComplete: (uc) => {
          this.bottomSheetRef.dismiss()
          // alert("Account has been created");
          // registerEmail.value = "",
          //   registerPassword.value = "",
          //   registerConfirmPassword.value = ""
        },
        onFail: (err) => {
          alert("Failed to create account")
        }
      })
    } else {
      alert("Failed to create account")
    }
  }

  //Login button
  onLogin(
    loginEmail: HTMLInputElement,
    loginPassword: HTMLInputElement
  ) {
    let email = loginEmail.value;
    let password = loginPassword.value;

    if (this.isNotEmpty(email) &&
      this.isNotEmpty(password)) {
      this.firebasetsAuth.signInWith({
        email: email,
        password: password,
        onComplete: (uc) => {
          // alert("Successfully login");
          //   loginEmail.value = "",
          //   loginPassword.value = ""
          this.bottomSheetRef.dismiss()
        },
        onFail: (err) => {
          alert("Failed to login")
        }
      })
    } else {
      alert("Failed to login")
    }
  }

  //Reset button
  onReset(
    resetEmail: HTMLInputElement
  ){
    let email = resetEmail.value;

    if (this.isNotEmpty(email)){
      this.firebasetsAuth.sendPasswordResetEmail({
        email: email,
        onComplete:(uc) =>{
          //alert("Password reset email sent to " + email);
          this.bottomSheetRef.dismiss();
          //resetEmail.value= "";
        }
      })
    }else{
      alert("Please enter your email")
    }
  }

  handleLogin() {
    this.authenticationService.authenticationService(this.username, this.password).subscribe((result)=> {
      this.invalidLogin = false;
      this.loginSuccess = true;
      this.successMessage = 'Login Successful.';
      //this.router.navigate(['/hello-world']);
      this.bottomSheetRef.dismiss();
    }, () => {
      this.invalidLogin = true;
      this.loginSuccess = false;
      //alert("Failed to login");
    });      
  }

  addforms(f: NgForm) {
    console.log(f.value)
    const postBody={
      firstname:f.value.firstname,
      lastname:f.value.lastname,
      username:f.value.username,
      email:f.value.email,
      password:f.value.password,
      role:"USER"
    }
    this.userservice.addUser(postBody).subscribe({
      error:data=>{
          console.log(data)
          if(data.status == "201"){
            alert("User registered");
          } else{
            console.log(data.status)
            alert("User not registered" + data);
            this.status = data.status
          }
      }  
    })
  }

  ViolatorLink() {
    //window.location.href = "http://localhost:8082/forgot-password";
    window.open("http://localhost:8082/forgot-password", "_blank");
  }



}

export enum AuthenticatorCompState {
  LOGIN,
  REGISTER,
  FORGOT_PASSWORD
}
