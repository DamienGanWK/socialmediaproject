import { Component } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { AuthenticatorComponent } from './authenticator/authenticator.component';
import { FirebaseTSAuth } from 'firebasets/firebasetsAuth/firebaseTSAuth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'socialmedia';
  auth=new FirebaseTSAuth;
  constructor(private loginsheet:MatBottomSheet){
    this.auth.listenToSignInStateChanges(
      user=>{
        this.auth.checkSignInState({
          whenSignedIn:user=>{
            //alert("Logged in")
          },
          whenSignedOut:user=>{
            //alert("Logged out")
          },
          whenSignedInAndEmailNotVerified:user=>{
            //alert("Please verify account in email")
          },
          whenSignedInAndEmailVerified:user=>{
            //alert("Account verified and logged in")
          },
          whenChanged:user=>{
            //alert("Changed")
          }
        })
      }
    )
  }
    
  loggedIn(){
    return this.auth.isSignedIn();
  }

  loginClick(){
    this.loginsheet.open(AuthenticatorComponent)
  }

  logoutClick(){
    this.auth.signOut();
  }
}
