import { Component, Input, OnInit } from '@angular/core';
import { PostData } from '../postfeed/postfeed.component';

@Component({
  selector: 'app-displaypost',
  templateUrl: './displaypost.component.html',
  styleUrls: ['./displaypost.component.css']
})
export class DisplaypostComponent implements OnInit {

@Input() postData!: PostData;

  constructor() { }

  ngOnInit(): void {
  }

}
