import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewserviceService {

  constructor(private http:HttpClient) { }

    //Create image post
    addImage(body:any){
  
      //return this.http.post('https://jsonplaceholder.typicode.com/users',body,{headers:head,params:p});
      return this.http.post('http://localhost:8080/uploadFile',body);
  
    }

  //Update
  updateUser(){
    const putBody={
      name:"Angel",
      id:1
    }

    const head = new HttpHeaders({
      'post':'1234',
      'data':'heyyy'
    })

    this.http.put('https://jsonplaceholder.typicode.com/users/3',putBody,{headers:head}).subscribe(data=>{
      console.log(data)
    });
  }

  //Create
  addUser(body:any){

    const head = new HttpHeaders({
      'post':'1234',
      'data':'heyyy'
    })

    const p = new HttpParams()
    .set('nums','1994')
    .set('num2','1997')

    //return this.http.post('https://jsonplaceholder.typicode.com/users',body,{headers:head,params:p});
    return this.http.post('http://localhost:8080/api/users',body,{headers:head});

  }

  //Read
  getUser(){
    const head = new HttpHeaders({
      'content':'name1',
      'pswd':'******'
    });

    const parameters = new HttpParams()
    .set('webpNum','1')
    .set('webpType','android')

    return this.http.get('https://jsonplaceholder.typicode.com/users',{headers:head,params:parameters})
  }

  //Delete
  deleteUser(i:any){
    const h1= new HttpHeaders({
      'delete20':'Rem',
      'delete100':'Emilia'
    })
    return this.http.delete('https://jsonplaceholder.typicode.com/users/' + i,{headers:h1})
  }
}
