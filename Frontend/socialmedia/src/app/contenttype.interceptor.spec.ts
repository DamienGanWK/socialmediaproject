import { TestBed } from '@angular/core/testing';

import { ContenttypeInterceptor } from './contenttype.interceptor';

describe('ContenttypeInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      ContenttypeInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: ContenttypeInterceptor = TestBed.inject(ContenttypeInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
