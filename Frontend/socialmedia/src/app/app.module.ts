import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FirebaseTSApp } from 'firebasets/firebasetsApp/firebaseTSApp';
import { environment } from 'src/environments/environment';
import { HomeComponent } from './home/home.component';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { AuthenticatorComponent } from './authenticator/authenticator.component';
import { ProfileComponent } from './profile/profile.component';
import { PostfeedComponent } from './postfeed/postfeed.component';
import { PostsComponent } from './posts/posts.component';
import { MatDialogModule } from '@angular/material/dialog';

import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './login/login.component';
import { HelloWorldComponent } from './hello-world/hello-world.component';

import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { LogoutComponent } from './logout/logout.component';
import { HttpInterceptorService } from './httpInterceptor.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DisplaypostComponent } from './displaypost/displaypost.component';

import { UserListComponent } from './user-list/user-list.component';
import { UserService } from './service/user.service';
import { UploadComponent } from './upload/upload.component';
import { ContenttypeInterceptor } from './contenttype.interceptor';
import { DisplayimagesComponent } from './displayimages/displayimages.component';
import { PostListComponent } from './post-list/post-list.component';
import { AdminComponent } from './admin/admin.component';
import {MatPaginatorModule} from '@angular/material/paginator';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AuthenticatorComponent,
    ProfileComponent,
    PostfeedComponent,
    PostsComponent,
    MenuComponent,
    LoginComponent,
    LogoutComponent,
    HelloWorldComponent,
    DisplaypostComponent,
    UserListComponent,
    UploadComponent,
    DisplayimagesComponent,
    PostListComponent,
    AdminComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    MatBottomSheetModule,
    MatIconModule,
    MatCardModule,
    MatDialogModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatPaginatorModule

  ],
  providers: [

    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    FirebaseTSApp.init(environment.firebaseConfig)
  }
}
