import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdmintrationRoutingModule } from './admintration-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdmintrationRoutingModule
  ]
})
export class AdmintrationModule { }
