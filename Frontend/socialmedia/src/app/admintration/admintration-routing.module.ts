import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from '../admin/admin.component';
import { PostListComponent } from '../post-list/post-list.component';
import { UserListComponent } from '../user-list/user-list.component';

const routes: Routes = [
  { path: '', children :[
    { path : 'home', component : AdminComponent },
    { path : 'users', component : UserListComponent },
    { path : 'posts', component : PostListComponent },
    { path : '', redirectTo : 'admin', pathMatch : 'full' }
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdmintrationRoutingModule { }
