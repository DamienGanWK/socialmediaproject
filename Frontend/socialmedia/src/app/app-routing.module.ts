import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { PostfeedComponent } from './postfeed/postfeed.component';
import { HelloWorldComponent } from './hello-world/hello-world.component';
import { DisplaypostComponent } from './displaypost/displaypost.component';
import { UserListComponent } from './user-list/user-list.component';
import { UploadComponent } from './upload/upload.component';
import { PostListComponent } from './post-list/post-list.component';
import { RouteGuardService } from './route-guard.service';

const routes: Routes = [
  { 
    path: 'admin', 
    canActivate : [RouteGuardService],
    loadChildren: () => import('./admintration/admintration.module').then(m => m.AdmintrationModule) 
  },
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: 'hello-world',
    component: HelloWorldComponent
  },
  {
    path: 'logout',
    component: HomeComponent
  },
  {
    path: "**",
    component: HomeComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
